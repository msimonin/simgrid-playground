#include <simgrid/s4u.hpp>

XBT_LOG_NEW_DEFAULT_CATEGORY(sleep_until, "sleep until example");

static void test_sleep_until()
{
    double next_time = 0;
    double time = 0;
    while (time < 0.1)
    {
        next_time = next_time + 1e-6;
        XBT_INFO("Je vais au temps %f", next_time);
        simgrid::s4u::this_actor::sleep_until(next_time);
        time = simgrid::s4u::Engine::get_clock();
        XBT_INFO("J'aimerais être au temps %f et je suis au temps %f", next_time, time);
        XBT_INFO("----");
    }
}

int main(int argc, char *argv[])
{
    simgrid::s4u::Engine e(&argc, argv);
    xbt_assert(argc != 1, "Usage: %s platform_file \n", argv[0]);

    /* Register the functions representing the actors */
    //  e.register_function("test_sleep_until", &test_sleep_until);

    /* Load the platform description and then deploy the application */
    e.load_platform(argv[1]);
    //  e.load_deployment(argv[2]);
    simgrid::s4u::Actor::create("receiver", simgrid::s4u::Host::by_name("Fafard"), test_sleep_until);

    /* Run the simulation */
    e.run();

    XBT_INFO("Simulation is over");

    return 0;
}
